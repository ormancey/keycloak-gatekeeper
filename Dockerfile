FROM alpine:3.8
MAINTAINER Emmanuel Ormancey <emmanuel.ormancey@cern.ch>

ENV NAME keycloak-gatekeeper

LABEL Name=keycloak-gatekeeper \
      Release=https://gitlab.cern.ch/ormancey/keycloak-gatekeeper \
      Url=https://gitlab.cern.ch/ormancey/keycloak-gatekeeper \
      Help=https://issues.jboss.org/projects/KEYCLOAK

RUN apk add --no-cache ca-certificates curl tar

WORKDIR "/opt"

RUN curl -fssL "https://gitlab.cern.ch/ormancey/keycloak-gatekeeper/raw/master/keycloak-gatekeeper-linux-amd64.tar.gz" | tar -xz && chmod +x /opt/$NAME

RUN apk del curl tar

EXPOSE 5000

ENTRYPOINT [ "/opt/keycloak-gatekeeper" ]