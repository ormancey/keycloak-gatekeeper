# Keycloak-gatekeeper for Openshift Pods

### Origin Dockerfile from 
https://github.com/jboss-dockerfiles/keycloak/blob/master/gatekeeper/Dockerfile


### Deploy in OC
oc new-app https://gitlab.cern.ch/ormancey/keycloak-gatekeeper.git

Add config map with environment variables:
```
PROXY_DISCOVERY_URL https://keycloak.cern.ch/auth/realms/master
PROXY_LISTEN        0.0.0.0:5000
PROXY_CLIENT_ID     test-wordpress
PROXY_CLIENT_SECRET xxxxx
PROXY_REDIRECTION_URL https://test-wordpress.web.cern.ch
PROXY_UPSTREAM_URL  http://test-wordpress.test-wordpress.svc:8080
```

Add in deployment YAML config:
```
      - args:
            - '--preserve-host'
            - '--verbose'
```

Per path cern_roles match:
```
- '--resources="uri=/wp-login.php|roles=wp_admin"'
```



### Using the Patched version
#### Patching keycloak-gatekeeper to use cern_roles as claim for roles
doc.go (63): 
```
   claimResourceRoles      = "cern_roles" // "roles" changed by EO
   claimResourceRolesRealm = "roles"      // added by EO
```
user_context.go (59):
```
	if roles, found := realmRoles[claimResourceRolesRealm]; found {
```	
server_test.go (595):
```
	"cern_roles": roles, // was "roles": roles,    changed by EO
```
